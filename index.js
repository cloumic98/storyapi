const cors = require("cors");
const app = require("express")()
const port = 3000
const history = {
    1: 'Lors d\'une soirée #student#, #student# et #student#, on décidé de faire un #game# ce qui les a conduis #place#',
    2: 'Le célebre ouvrage #book# écrit par #student# traite de la relation ambigue entre le jeune #student# et #animal# à été vendu à des millions d\'exemplaire',
    3: '#student# et #student# ont l\'honneur de vous inviter à leurs mariage qui aura lieu le 22/02/2069 #place#',
    4: '#student# et #student# ont l\'honneur de vous annoncer la naissance de leurs fille #student# croisement entre #animal# et #animal#',
    5: 'Le livre #book# est devenu la sainte écriture des #student#iens qui se distingue grâce à leurs têtes de #animal#',
    6: 'Il était une fois #place#, une jeune femme nommée #student#, qui trouva par hasard #animal# qui lui rappela #film# qu\'elle aimait tant.',
    7: 'Aujourd\'hui #student# et #student# ont fait #game#. Ils ont décidé d\'en faire une chanson, #music#. Qui deviendra un succès planétaire.',
    8: 'Pendant ce temps à l\'école 404, l\'atelier #game# a permis de souder #student#, #student# et #student#, les couples se forment.',
    9: 'Un groupe punk nommé #music#, est venu #place# afin d\'y déguster le célèbre #animal# du chef cuisinier #student#.',
    10: 'Un nouveau langage de programmation, le #student#, créé par #animal#, a fait l\'objet d\'un reportage : #film# présenté au festival #place#.',
    11: 'Ici la voix, le secret de #student# a été découvert par #student#. Il a égorgé #animal# à l\'aide du vinyl, dont le célèbre titre : #music# qui depuis est devenue l\'hymne national #place#.',
    12: 'Breaking NEWS..... #place# a été envahi par #student#, l\'ONU demande à ce que le conflit se règle par une partie de #game#.',
    13: 'ALERTE ENLEVEMENT..... #student#, 5 ans et demi a été enlevé. Il a été aperçu pour la dernière fois à #place# et regardais #film#.'
}

const animals = {
    1: 'le tigre',
    2: 'le lion',
    3: 'le chien',
    4: 'la loutre',
    5: 'le chat',
    6: 'le poisson pacu',
    7: 'le narval',
    8: 'la fourmi panda',
    9: 'la grenouille violette',
    10: 'la stomatopoda',
    11: 'le papillon caniche du',
    12: 'le tenrec strié'
}

const students = {
    1: 'Mathéo',
    2: 'Kevin',
    3: 'Franck',
    4: 'Mathieu',
    5: 'Thomas',
    6: 'Jordan',
    7: 'Maud',
    8: 'Bastien',
    9: 'Jonathan',
    10: 'Rudy',
    11: 'Guillaume',
    12: 'Florent'
}

const places = {
    1: 'a Collioure',
    2: 'au Fucking blue boys',
    3: 'a L\'école 404',
    4: 'dans la Forêt de Grimbosq',
    5: 'a Ganapati Formation',
    6: 'a la Corée du nord',
    7: 'Chez sa mère',
    8: 'a Ouistreham',
    9: 'au mcdo',
    10: 'a Disney',
    11: 'a Dashau',
    12: 'a Bangkok'
}

const musics = {
    1: 'Baby Shark',
    2: 'Ziguezon Zinzon',
    3: 'On fait tourner les quequetes',
    4: 'Les Sardines',
    5: 'Californication',
    6: 'Can\'t Stop',
    7: 'Back In Black',
    8: 'Rocketman',
    9: 'Quand il pète il trou son slip',
    10: 'Je lui mettrai bien une cartouche',
    11: 'Le lac du connemara',
    12: 'J\'ai les bonbon qui font des bon !'
}

const games = {
    1: 'CSGO',
    2: 'Uno',
    3: 'La levrette claqué',
    4: 'Le loup garou',
    5: 'LOL',
    6: 'WOW',
    7: 'Marco Pollo',
    8: 'Petanque',
    9: 'la chasse à la galinette cendré',
    10: 'la savonette',
    11: 'zizi panpan',
    12: 'le lancer de nain'
}

const films = {
    1: 'American History X',
    2: 'Blanche fesse et les 7 nains',
    3: 'Total reball',
    4: 'Non d\'une pipe',
    5: 'Harry Potter Contre Attaque',
    6: 'La rafle',
    7: 'Raby Jacob',
    8: 'Jésus Crie le retour',
    9: 'Katsuni et les 9 bambous',
    10: 'Le prof de géographie',
    11: 'Mobi Dick',
    12: 'La chatte de la mère Michèle'
}

const books = {
    1: 'Le prof de géographie',
    2: 'La bible',
    3: 'Harry Potter 1',
    4: 'Voyage au centre de la terre',
    5: 'Mein Kamph',
    6: 'Kamasutra',
    7: 'Le coran',
    8: 'Développer pour les nuls',
    9: '50 Nuances de Grey',
    10: 'Le seigneur des anneaux',
    11: 'Le Hobbit',
    12: '1984'
}

app.use(cors())

app.get('/getstory', (req, res) => {
    let randomPhrase = getRandomPhrase()

    res.json({ data: randomPhrase })
})

/*app.get('/gethistorique', (req, res) => {
    res.json({
        data: history
    })
})*/

app.listen(port, () => {
    console.log(`L'api démarre sur localhost:${port}`)
})

function getRandomPhrase(obj) {
    let phrase =  randomProperty(history)

    let type = [
        { type: "#music#", obj: musics },
        { type: "#film#", obj: films },
        { type: "#place#", obj: places },
        { type: "#game#", obj: games },
        { type: "#student#", obj: students },
        { type: "#animal#", obj: animals },
        { type: "#book#", obj: books }
    ]

    type.forEach((v) => {
        while(phrase.includes(v.type)) {
            phrase = phrase.replace(v.type, randomProperty(v.obj))
        }
    })

    return phrase
}

let randomProperty = function (obj) {
    let keys = Object.keys(obj)
    return obj[keys[ keys.length * Math.random() << 0]]
}